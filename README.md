# Slack Export SQLite Viewer

This project lets you view Slack exports. Slack permits you to download your
workspace's entire history as a zipfile full of JSON files, but those are hard
to view without a viewer tool like this.

## Current status

This is a work in progress. Currently, it can parse and render very basic static
HTML views of Slack exports.

## Usage

TODO

### Future features desired

Here are some features I'd like to add:

0. Serve HTML.
1. Searching over messages
2. Split channel pages up by month or year to make them easier to load.
3. Better rendering of Slack features (e.g. emojis, files, and link snippets)
4. Automatic periodic download of recent Slack exports

## Why not Slack Export Viewer?

There already exists a project for viewing a Slack export,
<https://github.com/hfaran/slack-export-viewer>. Why might you use this project
instead?

The biggest difference is that this project is designed to minimize memory
usage, which should help run it on tiny servers, e.g. a [tiny
VPS](https://lowendbox.com/) or 0.5GiB AWS t3.nano instance. (It lowers memory
usage by parsing the Slack export into a SQLite database and rendering from
there.)

Second, this project can render the Slack export to static files, which makes it
easy to serve from a shared web host or other limited server.

Finally, being implemented in a faster language than Python, this project might
be faster than Slack Export Viewer, though I have not benchmarked it at all.

## License

Copyright (c) Leon Barrett, 2021.

This project is licensed under the GNU GPL version 3 or later (at your option).
