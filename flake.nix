{
  description = "";
  inputs.nixpkgs.url = github:NixOS/nixpkgs/nixpkgs-unstable;

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        let pkgs = nixpkgs.legacyPackages.${system}; in
        {
          devShell =
            pkgs.mkShell {
              buildInputs = with pkgs ; [
                cargo
                rustc
                rust-analyzer
                sqlite
                sqlite-interactive
              ];
            };
          packages = {
            default = pkgs.rustPlatform.buildRustPackage {
              # TODO find good names
              pname = "slack-sqlite";
              version = "0.0.1";
              src = ./.;
              cargoLock.lockFile = ./Cargo.lock;
              buildInputs = with pkgs ; [
                sqlite
              ];
            };
          };
        });
}
