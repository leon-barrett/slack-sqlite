-- This table is for global information, e.g. the workspace name.
CREATE TABLE workspace_metadata
(
    workspace TEXT NOT NULL PRIMARY KEY
);

CREATE TABLE users
(
    id TEXT NOT NULL PRIMARY KEY,
    display_name TEXT NOT NULL, -- profile display_name falling back to name
    is_bot INTEGER NOT NULL DEFAULT 0,
    image TEXT  -- currently a link to the user's 48x48 icon
);

CREATE TABLE channels
(
    -- No ID. The channels.json file includes the IDs, but the actual message JSON files don't include channel IDs, so they don't do us much good.
    name TEXT NOT NULL,
    topic TEXT,
    purpose TEXT,
    is_archived INTEGER NOT NULL DEFAULT 0
);

-- A table to join which users are in which channels.
CREATE TABLE channel_users
(
    user_id TEXT NOT NULL,
    channel_name TEXT NOT NULL,
    PRIMARY KEY (channel_name, user_id)
);

CREATE TABLE messages(
    client_msg_id TEXT NOT NULL PRIMARY KEY,  -- looks like a lower-case UUID.
    channel_name TEXT NOT NULL,
    user_id TEXT,  -- some messages, e.g. "A file, which can't be shown because your team is past the free storage limit, was commented on." do not have users.
    ts TEXT NOT NULL,  -- A Slack timestamp is a decimal number equal to the Unix timestamp plus a 6-digit fraction indicating microseconds.
    'type' TEXT NOT NULL,
    subtype TEXT,
    'text' TEXT NOT NULL,
    thread_ts TEXT  -- Some replies have a "root" element, but some do not, so we have to rely on the timestamp, indicating the thread root message's timestamp. Let's hope it's unique across all channel messages....
);
CREATE INDEX messages_idx1 ON messages(channel_name, ts);

-- Emoji reactions to messages. This could've been stored denormalized, since we only ever query for it by message, but I think this is okay. If performance requires it, it still might need to be denormalized.
CREATE TABLE reactions (
    message_id TEXT NOT NULL,  -- might've named this client_msg_id, but I think the JSON source used message_id
    user TEXT NOT NULL,
    name TEXT NOT NULL,
    PRIMARY KEY (message_id, user, name)
);
CREATE INDEX reactions_idx1 ON reactions(message_id);
