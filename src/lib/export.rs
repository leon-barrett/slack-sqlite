//! Tools for reading Slack exports.

use fallible_iterator::{convert, FallibleIterator, IteratorExt};
use globwalk::{glob, WalkError};
use indicatif::ProgressBar;
use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use uuid;
use zip::read::ZipFile;
use zip::ZipArchive;

use std::convert::Infallible;
use std::error::Error;
use std::fs::File;
use std::io::{BufReader, Read};
use std::path::{Path, PathBuf};

/// A Slack export on the local filesystem, either a zip file or a decompressed
/// export.
pub struct SlackExport {
    pub path: PathBuf,
}

fn errconv(_e: Infallible) -> Box<dyn Error> {
    Box::<dyn Error>::from("this cannot happen")
}

impl SlackExport {
    /// Open a Slack export.
    pub fn open(path: &Path) -> SlackExport {
        // TODO validate export
        SlackExport {
            path: path.to_owned(),
        }
    }

    /// Get the workspace name (from the filename).
    pub fn workspace(&self) -> Option<String> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"(.*) Slack export .*").unwrap();
        }
        self.path
            .file_name()
            .and_then(|n| n.to_str())
            .and_then(|n| RE.captures(n))
            .map(|c| c[1].to_string())
    }

    /// True iff this is a zipfile (the path ends in .zip).
    pub fn is_zip(&self) -> bool {
        self.path.extension().and_then(|s| s.to_str()) == Some("zip")
    }

    /// Load the users.
    pub fn users(&self) -> Result<Vec<User>, Box<dyn Error>> {
        Ok(if self.is_zip() {
            let mut zip = ZipArchive::new(File::open(&self.path)?)?;
            let result = serde_json::from_reader(BufReader::new(zip.by_name("users.json")?))?;
            result
        } else {
            serde_json::from_reader(BufReader::new(File::open(self.path.join("users.json"))?))?
        })
    }

    /// Load the channels.
    pub fn channels(&self) -> Result<Vec<Channel>, Box<dyn Error>> {
        if self.is_zip() {
            let mut zip = ZipArchive::new(File::open(&self.path)?)?;
            let result = serde_json::from_reader(BufReader::new(zip.by_name("channels.json")?))?;
            Ok(result)
        } else {
            let mut path = self.path.to_owned();
            path.push("channels.json");
            let file = File::open(path)?;
            let reader = BufReader::new(file);
            let result = serde_json::from_reader(reader)?;
            Ok(result)
        }
    }

    /// Load the messages as a streaming FallibleIterator.
    ///
    /// # Arguments
    ///
    /// * `show_progress_bar` - Whether to show a progress bar.
    pub fn messages<'a>(
        &'a self,
        show_progress_bar: bool,
    ) -> Result<
        Box<dyn FallibleIterator<Item = MessageWithChannel, Error = Box<dyn Error>> + 'a>,
        Box<dyn Error>,
    > {
        if self.is_zip() {
            Ok(Box::new(ZipMessageIterator::new(
                &self.path,
                show_progress_bar,
            )?))
        } else {
            let s = self.path.to_str().ok_or("invalid path")?;
            let files: Vec<PathBuf> = glob(&format!("{}/*/????-??-??.json", s))?
                .map(|f| f.map(|p| p.path().to_owned()))
                .collect::<Result<Vec<PathBuf>, WalkError>>()?;
            let progress = make_progress_bar(files.len() as u64, show_progress_bar);
            Ok(Box::new(
                progress
                    .wrap_iter(files.into_iter())
                    .into_fallible()
                    .map_err(errconv)
                    .flat_map(|path| {
                        let chan = path_channel(&path)?;
                        let filename = path.to_str().unwrap_or("unknown").to_owned();
                        let f = File::open(path)?;
                        Ok(read_messages(f, chan).map_err(|e| {
                            format!("error reading message from file {}: {}", filename, e)
                        })?)
                    }),
            ))
        }
    }
}

/// Make a progress bar, possibly hidden.
fn make_progress_bar(length: u64, show_progress_bar: bool) -> ProgressBar {
    if show_progress_bar {
        ProgressBar::new(length)
    } else {
        ProgressBar::hidden()
    }
}

/// An iterator over messages in a Slack export zip archive.
struct ZipMessageIterator {
    zip: ZipArchive<File>,
    file_names: Box<dyn Iterator<Item = String>>,
    items: Box<dyn Iterator<Item = MessageWithChannel>>,
}

impl ZipMessageIterator {
    pub fn new(
        filename: &Path,
        show_progress_bar: bool,
    ) -> Result<ZipMessageIterator, Box<dyn Error>> {
        let zip = ZipArchive::new(File::open(filename)?)?;
        let re = Regex::new(r"[^/]*/[^/]*\.json")?;
        let file_names_vec: Vec<String> = zip
            .file_names()
            .filter(|f| re.is_match(f))
            .map(str::to_string)
            .collect();
        let progress = make_progress_bar(file_names_vec.len() as u64, show_progress_bar);
        Ok(ZipMessageIterator {
            zip,
            file_names: Box::new(progress.wrap_iter(file_names_vec.into_iter())),
            items: Box::new(vec![].into_iter()),
        })
    }
}

impl FallibleIterator for ZipMessageIterator {
    type Item = MessageWithChannel;
    type Error = Box<dyn Error>;

    fn next(&mut self) -> Result<Option<Self::Item>, Self::Error> {
        loop {
            if let Some(message) = self.items.next() {
                // iterate within current file
                return Ok(Some(message));
            } else if let Some(filename) = self.file_names.next() {
                lazy_static! {
                    static ref RE: Regex = Regex::new(r"\d{4}-\d{2}-\d{2}.json").unwrap();
                }
                if RE.find(&filename).is_none() {
                    continue;
                }
                // iterate in next file
                self.items = Box::new(
                    read_messages_from_zip_file(self.zip.by_name(&filename)?)?.into_iter(),
                );
            } else {
                // iteration done
                return Ok(None);
            }
        }
    }
}

/// Read messages from a zip file.
fn read_messages_from_zip_file(file: ZipFile) -> Result<Vec<MessageWithChannel>, Box<dyn Error>> {
    let filename = file
        .enclosed_name()
        .and_then(Path::to_str)
        .ok_or("malformed zip file")?
        .to_owned();
    let channel = path_channel(&file.enclosed_name().ok_or("malformed zip file")?)?;
    let reader = BufReader::new(file);
    let messages: Vec<Message> = serde_json::from_reader(reader)
        .map_err(|e| format!("error reading message from file {}: {}", filename, e))?;
    Ok(messages
        .into_iter()
        .map(|message| MessageWithChannel {
            message,
            channel: channel.clone(),
        })
        .collect())
}

/// Read messages from a file, as a FallibleIterator for flat_map.
fn read_messages<R>(
    file: R,
    channel: String,
) -> Result<impl FallibleIterator<Item = MessageWithChannel, Error = Box<dyn Error>>, Box<dyn Error>>
where
    R: Read,
{
    let reader = BufReader::new(file);
    let fmessages: Vec<Message> = serde_json::from_reader(reader)?;
    Ok(convert(fmessages.into_iter().map(move |message| {
        Ok(MessageWithChannel {
            message,
            channel: channel.clone(),
        })
    })))
}

/// Get the channel name from the path
fn path_channel<P>(path: &P) -> Result<String, Box<dyn Error>>
where
    P: AsRef<Path>,
{
    Ok(path
        .as_ref()
        .parent()
        .ok_or("internal parsing channel file path")?
        .file_name()
        .ok_or("internal parsing channel file path")?
        .to_string_lossy()
        .to_string())
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "strict", serde(deny_unknown_fields))]
pub struct UserProfile {
    pub title: Option<String>,
    pub phone: Option<String>,
    pub skype: Option<String>,
    pub real_name: Option<String>,
    pub real_name_normalized: Option<String>,
    pub display_name: Option<String>,
    pub display_name_normalized: Option<String>,
    pub status_text: Option<String>,
    pub status_text_canonical: Option<String>,
    pub status_emoji: Option<String>,
    pub status_expiration: Option<u64>,
    pub avatar_hash: Option<String>,
    pub image_original: Option<String>,
    #[serde(default)]
    pub is_custom_image: bool,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
    pub team: Option<String>,
    pub image_24: Option<String>,
    pub image_32: Option<String>,
    pub image_48: Option<String>,
    pub image_72: Option<String>,
    pub image_192: Option<String>,
    pub image_512: Option<String>,
    pub image_1024: Option<String>,
    // For bots
    pub api_app_id: Option<String>,
    #[serde(default)]
    pub always_active: bool,
    pub bot_id: Option<String>,
    // I haven't seen examples of these fields.
    pub fields: Value,
    pub status_emoji_display_info: Value,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[cfg_attr(feature = "strict", serde(deny_unknown_fields))]
pub struct User {
    pub id: String,
    pub team_id: String,
    pub name: String,
    pub real_name: Option<String>,
    #[serde(default)]
    pub is_admin: bool,
    #[serde(default)]
    pub is_owner: bool,
    #[serde(default)]
    pub is_primary_owner: bool,
    #[serde(default)]
    pub is_restricted: bool,
    #[serde(default)]
    pub is_ultra_restricted: bool,
    #[serde(default)]
    pub is_bot: bool,
    #[serde(default)]
    pub is_app_user: bool,
    pub updated: u64,
    #[serde(default)]
    pub is_email_confirmed: bool,
    pub profile: UserProfile,
    #[serde(default)]
    pub deleted: bool,
    pub color: Option<String>,
    pub tz: Option<String>,
    pub tz_label: Option<String>,
    #[serde(default)]
    pub tz_offset: i64,
    pub who_can_share_contact_card: Option<String>,
    #[serde(default)]
    pub is_invited_user: bool,
}

impl User {
    /// Get the user's display name, falling back to the user's name if not set.
    pub fn display_name(&self) -> String {
        self.profile
            .display_name
            .as_ref()
            .unwrap_or(&self.name)
            .to_string()
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "strict", serde(deny_unknown_fields))]
pub struct ChannelProperty {
    pub value: String,
    pub creator: String,
    pub last_set: u64,
}

#[derive(Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "strict", serde(deny_unknown_fields))]
pub struct Pin {
    pub id: String,
    #[serde(rename = "type")]
    pub type_: String,
    pub created: u64,
    pub user: String,
    pub owner: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "strict", serde(deny_unknown_fields))]
pub struct Channel {
    pub id: String,
    pub name: String,
    pub created: u64,
    pub creator: String,
    #[serde(default)]
    pub is_archived: bool,
    #[serde(default)]
    pub is_general: bool,
    pub members: Vec<String>,
    pub topic: ChannelProperty,
    pub purpose: ChannelProperty,
    #[serde(default)]
    pub pins: Vec<Pin>,
}

#[derive(Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "strict", serde(deny_unknown_fields))]
pub struct Reaction {
    pub name: String,
    pub users: Vec<String>,
    pub count: u64,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FileUpload {
    pub id: String,
    pub name: Option<String>,
    pub title: Option<String>,
    pub mimetype: Option<String>,
    pub filetype: Option<String>,
    pub pretty_type: Option<String>,
    pub user: Option<String>,
    pub mode: Option<String>,
    pub created: Option<u64>,
    pub timestamp: Option<u64>,
    pub size: Option<u64>,
    #[serde(default)]
    pub editable: bool,
    #[serde(default)]
    pub is_external: bool,
    #[serde(default)]
    pub is_public: bool,
    #[serde(default)]
    pub public_url_shared: bool,
    #[serde(default)]
    pub display_as_bot: bool,
    pub username: Option<String>,
    pub external_type: Option<String>,
    pub url_private: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "strict", serde(deny_unknown_fields))]
pub struct Edit {
    pub user: String,
    pub ts: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "strict", serde(deny_unknown_fields))]
pub struct Reply {
    pub user: String,
    pub ts: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[cfg_attr(feature = "strict", serde(deny_unknown_fields))]
pub struct Message {
    #[serde(rename = "type")]
    pub type_: String,
    // Maybe this should be an enum with explicit subtypes from
    // https://slack.com/help/articles/220556107-How-to-read-Slack-data-exports
    pub subtype: Option<String>,
    pub text: String,
    pub user: Option<String>,
    pub ts: String,
    pub team: Option<String>,
    pub user_team: Option<String>,
    pub source_team: Option<String>,
    #[serde(default)]
    pub upload: bool,
    #[serde(default)]
    pub display_as_bot: bool,
    #[serde(default)]
    pub files: Vec<FileUpload>,
    #[serde(default)]
    pub reactions: Vec<Reaction>,
    /// Currently this is not inspected.
    pub user_profile: Option<Value>,
    /// Currently this is not inspected.
    pub blocks: Option<Value>,
    /// Currently this is not inspected.
    pub attachments: Option<Value>,
    #[serde(default = "gen_uuid_string")]
    pub client_msg_id: String,
    pub edited: Option<Edit>,
    // from channel_join
    pub inviter: Option<String>,
    pub thread_ts: Option<String>,
    #[serde(default)]
    pub reply_count: u64,
    #[serde(default)]
    pub reply_users_count: u64,
    #[serde(default)]
    pub reply_users: Vec<String>,
    #[serde(default)]
    pub replies: Vec<Reply>,
    pub latest_reply: Option<String>,
    pub last_read: Option<String>,
    #[serde(default)]
    pub is_locked: bool,
    #[serde(default)]
    pub subscribed: bool,
    pub parent_user_id: Option<String>,
    pub bot_id: Option<String>,
    pub bot_profile: Option<Value>,
    pub root: Option<Box<Message>>,
    pub upload_reply_to: Option<String>,
    #[serde(default)]
    pub hidden: bool,
    pub name: Option<String>,
    pub old_name: Option<String>,
    // from channel_purpose
    pub purpose: Option<String>,
    // from channel_topic
    pub topic: Option<String>,
    #[serde(default)]
    pub is_hidden_by_limit: bool,
    #[serde(default)]
    pub x_files: Vec<String>,
    // from pinned_item
    pub item_type: Option<String>,
    // from reply_broadcast
    #[serde(default)]
    pub new_broadcast: bool,
    // from bot_message
    pub username: Option<String>,
}

/// A helper to fill in empty UUIDs by default.
fn gen_uuid_string() -> String {
    uuid::Uuid::new_v4().to_string()
}

/// A message along with the channel it's from (because that's in the filename).
pub struct MessageWithChannel {
    pub message: Message,
    pub channel: String,
}
