//! A crate to parse and render Slack exports using a Sqlite database.
//!
//! It can:
//! * parse a Slack export into a SQLite database
//! * render a database as an HTML site
//! * serve a database as HTML site (eventually including a search)
//!
//! It is designed to be memory-efficient, so it will be useful in tiny VPS
//! servers (e.g. free Google Cloud 256MB servers) or maybe on "cloud
//! functions".

mod export;
mod parse;
mod query;
mod render;

// TODO clean up
pub use export::*;
pub use parse::*;
pub use query::*;
pub use render::*;
