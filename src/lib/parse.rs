//! Tools for parsing a Slack export into a database.

use fallible_iterator::FallibleIterator;
use rusqlite::{self, Connection};
use serde::{Deserialize, Serialize};
use serde_rusqlite;

use super::export::*;
use std::error::Error;
use std::fs::remove_file;
use std::path::Path;

#[derive(Debug, Serialize, Deserialize)]
pub struct DBUser {
    pub id: String,
    pub display_name: String,
    pub is_bot: bool,
    pub image: Option<String>,
}

impl DBUser {
    pub fn from_user(user: User) -> DBUser {
        let display_name = user.display_name();
        DBUser {
            id: user.id,
            display_name,
            is_bot: user.is_bot,
            image: user.profile.image_48.or(user.profile.image_original),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DBChannel {
    // No ID. When we insert messages, we don't get channel IDs.
    pub name: String,
    pub topic: String,
    pub purpose: String,
    pub is_archived: bool,
}

impl DBChannel {
    pub fn from_channel(channel: &Channel) -> DBChannel {
        DBChannel {
            name: channel.name.clone(),
            is_archived: channel.is_archived,
            topic: channel.topic.value.clone(),
            purpose: channel.purpose.value.clone(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DBChannelUser {
    user_id: String,
    channel_name: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DBWriteMessage {
    client_msg_id: String,
    channel_name: String,
    user_id: Option<String>,
    ts: String,
    #[serde(rename = "type")]
    type_: String,
    subtype: Option<String>,
    text: String,
    thread_ts: Option<String>,
}

impl DBWriteMessage {
    pub fn from_message(message: &MessageWithChannel) -> DBWriteMessage {
        DBWriteMessage {
            client_msg_id: message.message.client_msg_id.clone(),
            channel_name: message.channel.to_owned(),
            user_id: message.message.user.clone(),
            ts: message.message.ts.clone(),
            type_: message.message.type_.clone(),
            subtype: message.message.subtype.clone(),
            text: message.message.text.clone(),
            thread_ts: message.message.thread_ts.clone(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct DBMessageReaction {
    message_id: String,
    user: String,
    name: String,
}

pub fn prepare_db(conn: &mut rusqlite::Connection) -> rusqlite::Result<()> {
    conn.execute_batch(include_str!("db.sql"))
}

#[derive(Debug, Serialize, Deserialize)]
pub struct WorkspaceMetadata {
    pub workspace: String,
}

pub fn insert_workspace_metadata(
    conn: &mut rusqlite::Connection,
    workspace_metadata: WorkspaceMetadata,
) -> Result<(), Box<dyn Error>> {
    conn.prepare("INSERT INTO workspace_metadata (workspace) values (:workspace)")?
        .execute(
            serde_rusqlite::to_params_named(workspace_metadata)?
                .to_slice()
                .as_slice(),
        )?;
    Ok(())
}

pub fn insert_users<T>(conn: &mut rusqlite::Connection, users: T) -> Result<(), Box<dyn Error>>
where
    T: IntoIterator<Item = User>,
{
    let tx = conn.transaction()?;
    {
        let mut insert_user = tx.prepare(
            "INSERT INTO users (id, display_name, is_bot, image) values (:id, :display_name, :is_bot, :image)",
        )?;
        for user in users {
            let db_user = &DBUser::from_user(user);
            insert_user.execute(
                serde_rusqlite::to_params_named(db_user)?
                    .to_slice()
                    .as_slice(),
            )?;
        }
    }
    tx.commit()?;
    Ok(())
}

pub fn insert_channels<T>(
    conn: &mut rusqlite::Connection,
    channels: T,
) -> Result<(), Box<dyn Error>>
where
    T: IntoIterator<Item = Channel>,
{
    let tx = conn.transaction()?;
    {
        let mut insert_channel = tx.prepare(
            "INSERT INTO channels (name, topic, purpose, is_archived) values (:name, :topic, :purpose, :is_archived)",
        )?;
        let mut insert_channel_user = tx.prepare(
            "INSERT INTO channel_users (user_id, channel_name) values (:user_id, :channel_name)",
        )?;
        for channel in channels {
            let db_channel = &DBChannel::from_channel(&channel);
            insert_channel.execute(
                serde_rusqlite::to_params_named(db_channel)?
                    .to_slice()
                    .as_slice(),
            )?;
            for user_id in channel.members {
                insert_channel_user.execute(
                    serde_rusqlite::to_params_named(DBChannelUser {
                        user_id,
                        channel_name: channel.name.clone(),
                    })?
                    .to_slice()
                    .as_slice(),
                )?;
            }
        }
    }
    tx.commit()?;
    Ok(())
}

pub fn insert_messages<T>(
    conn: &mut rusqlite::Connection,
    mut messages: T,
) -> Result<(), Box<dyn Error>>
where
    T: FallibleIterator<Item = MessageWithChannel, Error = Box<dyn Error>>,
{
    let tx = conn.transaction()?;
    {
        let mut insert_message = tx.prepare(
            "INSERT INTO messages (client_msg_id, channel_name, user_id, ts, type, subtype, 'text', thread_ts) values (:client_msg_id, :channel_name, :user_id, :ts, :type, :subtype, :text, :thread_ts) ON CONFLICT(client_msg_id) DO NOTHING",
        )?;
        let mut insert_message_reaction = tx.prepare(
            "INSERT INTO reactions (message_id, user, name) values (:message_id, :user, :name)",
        )?;
        while let Some(message) = messages.next()? {
            insert_message.execute(
                serde_rusqlite::to_params_named(DBWriteMessage::from_message(&message))?
                    .to_slice()
                    .as_slice(),
            )?;
            for reaction in message.message.reactions {
                for user in reaction.users {
                    insert_message_reaction.execute(
                        serde_rusqlite::to_params_named(DBMessageReaction {
                            message_id: message.message.client_msg_id.clone(),
                            user,
                            name: reaction.name.clone(),
                        })?
                        .to_slice()
                        .as_slice(),
                    )?;
                }
            }
        }
    }
    tx.commit()?;
    Ok(())
}

/// Parse from a Slack export (either as a zip file or as a directory) into a
/// SQLite database.
///
/// # Arguments
///
/// * `slack_export_path`  - A Slack export to parse, either a zip file or a
///                          directory
/// * `db_path`            - A SQLite database to write to
/// * `overwrite_database` - If true and the database already exists, it will be
///                          overwritten.
/// * `workspace_name`     - An optional workspace name to include in the parse.
///                          If not provided, it's extracted from the export
///                          filename
/// * `show_progress_bar`  - Whether to show a console progress bar
pub fn parse<P1: AsRef<Path>, P2: AsRef<Path>>(
    slack_export_path: P1,
    db_path: P2,
    overwrite_database: bool,
    workspace_name: Option<String>,
    show_progress_bar: bool,
) -> Result<Connection, Box<dyn Error>> {
    let slack_export_path = slack_export_path.as_ref();
    let db_path = db_path.as_ref();
    println!(
        "Parsing Slack history from export \"{}\"",
        slack_export_path.display(),
    );
    println!("Writing to database \"{}\"", db_path.display());
    let export = SlackExport::open(slack_export_path);
    if db_path.exists() {
        if overwrite_database {
            print!("Force removing database \"{}\" ...", db_path.display());
            remove_file(db_path).map_err(|e| format!("error deleting database: {}", e))?;
            println!(" done.");
        } else {
            Err("db already exists; not overwriting")?;
        }
    }
    println!("Populating database...");
    let mut db = rusqlite::Connection::open(db_path)?;
    prepare_db(&mut db).unwrap();
    insert_workspace_metadata(
        &mut db,
        WorkspaceMetadata {
            workspace: workspace_name
                .or(export.workspace())
                .unwrap_or("workspace unknown".to_string()),
        },
    )?;
    let users = export
        .users()
        .map_err(|e| format!("Error loading users: {}", e))?;
    insert_users(&mut db, users.to_vec())
        .map_err(|e| format!("Error writing users to database: {}", e))?;
    insert_channels(
        &mut db,
        export
            .channels()
            .map_err(|e| format!("Error loading channels: {}", e))?,
    )
    .map_err(|e| format!("Error writing channels to database: {}", e))?;
    insert_messages(
        &mut db,
        export
            .messages(show_progress_bar)
            .map_err(|e| format!("Error loading messages: {}", e))?,
    )
    .map_err(|e| format!("Error writing messages to database: {}", e))?;
    println!("Done.");
    Ok(db)
}
