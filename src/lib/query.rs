use crate::parse::DBUser;
use rusqlite;
use serde_rusqlite;

pub fn query_users(db: &rusqlite::Connection) -> Result<Vec<DBUser>, serde_rusqlite::Error> {
    let mut users_query = db.prepare("SELECT * FROM users;")?;
    let query = users_query.query([])?;
    serde_rusqlite::from_rows::<DBUser>(query).collect()
}
