use chrono::offset::{TimeZone, Utc};
use chrono::{DateTime, SecondsFormat};
extern crate horrorshow;
use horrorshow::prelude::*;
use lazy_static::lazy_static;
use regex::{Captures, Regex};
use rusqlite;
use serde::{de, Deserialize, Deserializer};
use serde_rusqlite;

use std::collections::HashMap;
use std::error::Error;
use std::io::Write;

use crate::parse::*;

pub fn query_workspace(db: &rusqlite::Connection) -> Result<String, Box<dyn Error>> {
    Ok(serde_rusqlite::from_rows::<String>(
        db.prepare("SELECT workspace FROM workspace_metadata")?
            .query([])?,
    )
    .last()
    .ok_or("workspace not found")??)
}

pub fn query_channels(db: &rusqlite::Connection) -> serde_rusqlite::Result<Vec<DBChannel>> {
    serde_rusqlite::from_rows::<DBChannel>(db.prepare("SELECT * FROM channels")?.query([])?)
        .collect()
}

fn deserialize_ts<'de, D>(d: D) -> Result<DateTime<Utc>, D::Error>
where
    D: Deserializer<'de>,
{
    let s = String::deserialize(d)?;
    match s.split(".").collect::<Vec<&str>>()[..] {
        [secs_s, usecs_s] => {
            let secs: i64 = secs_s.parse().map_err(de::Error::custom)?;
            let usecs: u32 = usecs_s.parse().map_err(de::Error::custom)?;
            Ok(Utc.timestamp(secs, usecs * 1000))
        }
        _ => Err(de::Error::custom(format!(
            "ts had unexpected formatting: {}",
            s
        ))),
    }
}

#[derive(Debug, Deserialize)]
// Fields type and subtype are currently unused, but as public members, others
// could later use them.
#[allow(dead_code)]
pub struct DBReadMessage {
    text: String,
    #[serde(rename = "type")]
    type_: String,
    subtype: Option<String>,
    #[serde(deserialize_with = "deserialize_ts")]
    ts: DateTime<Utc>,
    #[serde(with = "serde_with::json::nested")]
    reactions: Option<HashMap<String, Vec<String>>>,
    display_name: Option<String>,
    image: Option<String>,
    in_thread: bool,
}

fn render_ts(ts: &DateTime<Utc>) -> String {
    ts.to_rfc3339_opts(SecondsFormat::Secs, true)
}

pub fn template_channels(
    writer: &mut dyn Write,
    workspace: &str,
    channels: &[DBChannel],
) -> Result<(), Box<dyn Error>> {
    Ok((horrorshow::html! {
        : horrorshow::helper::doctype::HTML;
        meta(charset = "UTF-8");
        html {
            head {
                style: css();
                title {
                    : "Slack history of " ;
                    : workspace;
                }
            }
            body {
                div(class="channels") {
                    a(href="index.html") {
                        h1 {
                            : "Slack history of ";
                            : workspace;
                        }
                    }
                    h2: "Channels";
                    ul {
                        @ for channel in channels {
                            li {
                                a(href=format!("channel/{}.html", channel.name)) : &channel.name;
                            }
                        }
                    }
                }
            }
        }
    })
    .write_to_io(writer)?)
}

fn css() -> String {
    "
    .channel_messages ul {
        list-style: none;
    }
    .time {
        color: #777;
    }
    .message {
        clear: both;
        margin: 0.5em;
    }
    .in_thread {
        clear: both;
        margin: 0.5em;
        margin-left: 4em;
    }
    img.user_image {
        float: left;
        margin: 0.5em;
    }
    .user {
        font-weight: bold;
    }
    .time {
        margin: 0.5em;
    }
    .text {
        display: block;
    }
    .reactions {
        font-size: 80%;
    }
    .reactions .reaction_users {
        font-style: italic;
    }
    "
    .to_string()
}

fn replace_user_references(users: &HashMap<&str, &DBUser>, message: String) -> String {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"<@([^>]+)>").expect("static regex failed");
    }
    RE.replace_all(&message, |c: &Captures| {
        format!(
            "@{}",
            users
                .get(&c[1])
                .map(|u| &u.display_name)
                .unwrap_or(&"unknown_user".to_string())
        )
    })
    .into()
}

pub fn template_channel_messages(
    writer: &mut dyn Write,
    workspace: &str,
    channel: &DBChannel,
    users: Vec<DBUser>,
    messages: impl Iterator<Item = Result<DBReadMessage, serde_rusqlite::Error>>,
) -> Result<(), horrorshow::Error> {
    let mut users_map: HashMap<&str, &DBUser> = HashMap::new();
    for user in users.iter() {
        users_map.insert(&user.id, &user);
    }
    (horrorshow::html! {
        : horrorshow::helper::doctype::HTML;
        meta(charset = "UTF-8");
        // TODO emojis
        // TODO sidebar
        // TODO eventually timezones
        html {
            head {
                style {
                    : css();
                }
                title {
                    : "Slack history of " ;
                    : workspace;
                    : ": ";
                    : &channel.name;
                }
            }
            body {
                div(class="channel_messages") {
                    a(href="../index.html"){
                        h1 {
                            : "Slack history of ";
                            : workspace;
                        }
                    }
                    h2(class="channel_name"){ : &channel.name; }
                    em {
                        p(class="channel_topic") {
                            : "Topic: ";
                            : &channel.topic;
                        }
                        p(class="channel_purpose") {
                            : "Purpose: ";
                            : &channel.purpose;
                        }
                    }
                    ul {
                        @ for message_result in messages {
                            @ if let Ok(message) = message_result {
                                li(class=(if message.in_thread {"message in_thread"} else {"message thread_root"})) {
                                    @ if let Some(image) = message.image {
                                        img(class="user_image", src=image, width="48", height="48");
                                    }
                                    span(class="byline") {
                                        span(class="user") {
                                            span(class="username") {
                                                : message.display_name.as_deref().unwrap_or("unknown_user");
                                            }
                                        }
                                        span(class="time") {
                                            : render_ts(&message.ts);
                                        }
                                    }
                                    span(class="text") {
                                        : replace_user_references(&users_map, message.text);
                                    }
                                    @ if let Some(reactions) = message.reactions {
                                        span(class="reactions") {
                                            @ for (reaction, users) in reactions {
                                                span {
                                                    : ":";
                                                    : reaction;
                                                    : ": ";
                                                    span(class="reaction_users") {
                                                        : users.join(", ");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    })
    .write_to_io(writer)
}
