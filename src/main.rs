//! A binary to parse and render Slack exports using Sqlite.

use clap::Parser;
use rusqlite;
use slack_export_sqlite_viewer::*;

use std::error::Error;
use std::path::PathBuf;

mod render;
mod serve;

use render::{render, Render};
use serve::{serve, Serve};

#[derive(Debug, Parser)]
#[command(
    name = "slack-sqlite",
    about = "Parse Slack export to sqlite and render it from there"
)]
enum Opt {
    Parse(Parse),
    Render(Render),
    Serve(Serve),
}

#[derive(Debug, Parser)]
pub struct Parse {
    /// The input Slack export, either a zip file or an unpacked directory.
    input: PathBuf,
    /// The output sqlite database.
    #[arg(long, default_value = "db.sqlite3")]
    db: PathBuf,
    /// If true, overwrite the database if it already exists.
    #[arg(long, short = 'f')]
    force: bool,
    /// Workspace name; defaults to extracted from export filename.
    #[arg(long)]
    workspace: Option<String>,
}

pub fn main_parse(
    Parse {
        input,
        db: db_path,
        force,
        workspace,
    }: Parse,
) {
    parse(input, db_path, force, workspace, true).unwrap();
}

#[actix_web::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let opt = Opt::parse();
    match opt {
        Opt::Parse(p) => Ok(main_parse(p)),
        Opt::Render(r) => Ok(render(r)),
        Opt::Serve(s) => serve(s).await,
    }
}
