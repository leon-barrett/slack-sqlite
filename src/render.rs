use clap::Parser;
use rusqlite::Connection;
use std::fs::{create_dir_all, remove_dir_all, File};
use std::io::{BufWriter, Write};
use std::path::{Path, PathBuf};
use tempfile::TempDir;

use crate::*;

/// Command-line options for rendering.
#[derive(Debug, Parser)]
pub struct Render {
    /// The input Slack export zip file, export directory, or parsed database.
    #[arg(long, short = 'i')]
    input: PathBuf,
    /// The output directory.
    #[arg(long)]
    output: PathBuf,
    /// If true, clear the output directory if it already exists.
    #[arg(long, short = 'f')]
    force: bool,
    /// Optionally render only a few channels. (useful for debugging)
    #[arg(long)]
    channels: Option<String>,
    // TODO LLL exclude channels, useful for filtering. Use clap::Args with #[group(required=false, multiple=false)]
}

fn write_channels(root: &Path, workspace: &str, channels: &[DBChannel]) {
    create_dir_all(root).expect("error creating output directory");
    let mut path = root.to_owned();
    path.push("index.html");
    println!("  rendering index");
    let mut outfile = BufWriter::new(File::create(path).expect("error creating output file"));
    template_channels(&mut outfile, workspace, channels).expect("error rendering channel list");
    outfile.flush().unwrap();
}

pub fn write_channel_writer(
    outfile: &mut impl Write,
    db: &rusqlite::Connection,
    workspace: &str,
    channel: &DBChannel,
) -> Result<(), Box<dyn Error>> {
    let users = query_users(db)?.into_iter().collect();
    let mut messages_query = db.prepare("
SELECT * FROM
  (
    SELECT
    false AS in_thread,
    messages.ts AS sort_ts, messages.subtype,
    users.image,
    users.display_name,
    messages.thread_ts,
    messages.ts,
    messages.text,
    messages.type,
    (
      SELECT json_group_object(reactions.name, json_array(users.display_name))
      FROM reactions
      LEFT JOIN users ON reactions.user = users.id
      WHERE reactions.message_id = messages.client_msg_id
    ) as reactions
    FROM messages LEFT JOIN users ON users.id = messages.user_id
    WHERE messages.channel_name = :channel_name AND (messages.thread_ts IS NULL OR messages.thread_ts = messages.ts)
  UNION ALL
    SELECT
    true AS in_thread,
    -- NOTE Leon removed the messages_thread_root on 2023-05-16 because it seemed really slow, and also unnecessary.
    messages.thread_ts AS sort_ts,
    -- messages_thread_root.ts AS sort_ts,
    messages.subtype,
    users.image,
    users.display_name,
    messages.thread_ts,
    messages.ts,
    messages.text,
    messages.type,
    (
      SELECT json_group_object(reactions.name, json_array(users.display_name))
      FROM reactions
      LEFT JOIN users ON reactions.user = users.id
      WHERE reactions.message_id = messages.client_msg_id
    ) as reactions
    FROM messages LEFT JOIN users ON users.id = messages.user_id
    -- LEFT JOIN messages AS messages_thread_root ON messages.thread_ts = messages_thread_root.thread_ts
    WHERE (
      messages.channel_name = :channel_name
      AND messages.thread_ts IS NOT NULL
      -- AND messages_thread_root.channel_name = :channel_name
      -- AND messages_thread_root.thread_ts = messages_thread_root.ts
      -- AND messages.ts != messages_thread_root.ts
    )
  )
ORDER BY sort_ts;
")?;
    let messages = serde_rusqlite::from_rows::<DBReadMessage>(
        messages_query.query(rusqlite::named_params! {":channel_name": &channel.name})?,
    );
    template_channel_messages(outfile, workspace, channel, users, messages)?;
    outfile.flush()?;
    Ok(())
}

fn write_channel(
    root: &Path,
    db: &rusqlite::Connection,
    workspace: &str,
    channel: &DBChannel,
) -> Result<(), Box<dyn Error>> {
    let mut path = root.to_owned();
    path.push("channel");
    create_dir_all(path.as_path())?;
    path.push(format!("{}.html", &channel.name));
    let mut outfile = BufWriter::new(File::create(path)?);
    write_channel_writer(&mut outfile, db, workspace, channel)
}

/// True iff the option contains the substring.
fn opt_contains(v: &Option<Vec<String>>, s: &str) -> bool {
    v.as_deref().map_or(true, |vv| vv.contains(&s.to_owned()))
}

/// Open an input database or parse a Slack export to a database.
fn open(input: impl AsRef<Path>) -> Result<Connection, Box<dyn Error>> {
    // I found that opening a file that's not a database was often ok. So I
    // force the database to open and check for errors by querying for channels.
    let try_conn = rusqlite::Connection::open(&input);
    if let Ok(db) = try_conn {
        if query_channels(&db).is_ok() {
            return Ok(db);
        }
    }
    println!("Looks like a raw Slack export; parsing it");
    let td = TempDir::with_prefix("slack-")?;
    let path = td.path().to_owned().join("slack.sqlite3");
    Ok(parse(&input, &path, true, None, true)?)
}

pub fn render(
    Render {
        input,
        output,
        force,
        channels: channels_str,
    }: Render,
) {
    println!(
        "Rendering Slack messages in channels {} from \"{}\" to directory \"{}\"",
        channels_str.as_deref().unwrap_or("*"),
        input.display(),
        output.display()
    );
    if !input.exists() {
        panic!("input {} does not exist", input.display());
    }
    // If it's not already parsed, parse it to a tempfile.
    let db = open(&input).expect("error opening input");
    let selected_channels: Option<Vec<String>> = match channels_str.as_deref() {
        None => None,
        Some("*") => None,
        Some(c) => Some(c.split(",").map(str::to_owned).collect()),
    };
    if output.exists() {
        if force {
            println!("  Removing non-empty output directory {}", output.display());
            remove_dir_all(output.as_path()).expect("error deleting output directory");
        } else {
            panic!(
                "output {} already exists; not overwriting",
                output.display()
            );
        }
    }
    let workspace = query_workspace(&db).expect("database error");
    let mut channels: Vec<DBChannel> = query_channels(&db)
        .expect("database error")
        .into_iter()
        .filter(|c| opt_contains(&selected_channels, &c.name))
        .collect();
    channels.sort_by(|c1, c2| c1.name.cmp(&c2.name));
    write_channels(&output, &workspace, &channels[..]);
    for channel in channels.iter() {
        println!("  rendering channel {}", channel.name);
        write_channel(&output, &db, &workspace, &channel).expect("error writing output");
    }
    println!("Done.");
}
