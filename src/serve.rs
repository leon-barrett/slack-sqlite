use actix_files;
use actix_web;
use clap::Parser;
use r2d2::PooledConnection;
use r2d2_sqlite::SqliteConnectionManager;
use serde::Deserialize;

use std::error::Error;
use std::fs::{create_dir_all, File};
use std::io::{BufWriter, Seek, SeekFrom, Write};
use std::ops::Deref;
use std::path::PathBuf;
use std::sync::Arc;
use tempfile::{tempfile, NamedTempFile, TempDir};

use crate::*;
use render::write_channel_writer;

#[derive(Debug, Parser)]
pub struct Serve {
    /// The input database.
    #[arg(long, default_value = "db.sqlite3")]
    db: PathBuf,
    /// The address to serve on
    #[arg(long, default_value = "localhost:8080")]
    address: String,
}

#[derive(Clone)]
struct State {
    pool: r2d2::Pool<SqliteConnectionManager>,
    dir: Arc<TempDir>,
}

/// Get a database pool from the shared state.
fn get_state_pool(
    state: &actix_web::web::Data<State>,
) -> actix_web::Result<PooledConnection<SqliteConnectionManager>> {
    state
        .pool
        .get()
        .map_err(|_| actix_web::error::ErrorInternalServerError("database connection error"))
}

/// Run the closure with a file, or skip it if already cached.
fn with_cached_file<F: FnOnce(&mut BufWriter<&File>) -> Result<(), Box<dyn Error>>>(
    dir: Arc<TempDir>,
    filename: &str,
    f: F,
) -> Result<File, Box<dyn Error>> {
    let file_path = dir.path().join(filename);
    if let Ok(cached) = File::open(file_path.as_path()) {
        Ok(cached)
    } else {
        // Use a tempfile and move it into place to avoid caching partial data.
        let mut tf = NamedTempFile::new_in(dir.path()).map_err(|_| "error creating tempfile")?;
        {
            let mut bf = BufWriter::new(tf.as_file());
            f(&mut bf)?;
            bf.flush()?;
        }
        tf.seek(SeekFrom::Start(0))?;
        create_dir_all(file_path.parent().ok_or("error creating cache dir")?)?;
        Ok(tf.persist(file_path).map_err(|_| "error saving cache")?)
    }
}

/// Render to a tempfile to serve.
// temporarily unused until search is possible
#[allow(dead_code)]
fn with_tempfile<F: FnOnce(&mut BufWriter<&File>) -> Result<(), Box<dyn Error>>>(
    f: F,
) -> Result<File, Box<dyn Error>> {
    let mut tf = tempfile().map_err(|_| "error creating tempfile")?;
    {
        let mut bf = BufWriter::new(&tf);
        f(&mut bf)?;
        bf.flush()?;
    }
    tf.seek(SeekFrom::Start(0))?;
    Ok(tf)
}

/// Path params for handle_channel
#[derive(Deserialize)]
struct HandleChannelPathParams {
    channel: String,
}

/// Render the channel index to the web server using a locally cached file.
#[actix_web::get("/")]
async fn handle_index(
    state: actix_web::web::Data<State>,
) -> actix_web::Result<impl actix_web::Responder> {
    let conn = get_state_pool(&state)?;
    let db: &rusqlite::Connection = conn.deref();
    let filename = "index.html";
    let f = with_cached_file(state.dir.clone(), filename, |mut f| {
        template_channels(&mut f, &query_workspace(&db)?, &query_channels(&db)?)
    })?;
    Ok(actix_files::NamedFile::from_file(f, filename))
}

/// Render a channel to the web server using a locally cached file.
#[actix_web::get("/channel/{channel}.html")]
async fn handle_channel(
    state: actix_web::web::Data<State>,
    path: actix_web::web::Path<HandleChannelPathParams>,
) -> actix_web::Result<impl actix_web::Responder> {
    let conn = get_state_pool(&state)?;
    let db: &rusqlite::Connection = conn.deref();
    let channel_filename = &format!("channels/{}.html", path.channel);
    let f = with_cached_file(state.dir.clone(), channel_filename, |mut f| {
        println!("render channel {}", path.channel);
        let db_channel: DBChannel = query_channels(&db)?
            .into_iter()
            .find(|c| c.name == path.channel)
            .ok_or(actix_web::error::ErrorNotFound("channel not found"))?;
        let workspace = query_workspace(&db)?;
        Ok(write_channel_writer(&mut f, &db, &workspace, &db_channel)?)
    })?;
    Ok(actix_files::NamedFile::from_file(f, channel_filename))
}

/// Add a body for 404 responses.
async fn not_found() -> actix_web::Result<impl actix_web::Responder> {
    Ok(actix_web::HttpResponse::NotFound().body("not found"))
}

/// Run the web server.
pub async fn serve(
    Serve {
        db: db_path,
        address,
    }: Serve,
) -> Result<(), Box<dyn Error>> {
    let manager = SqliteConnectionManager::file(db_path.clone());
    let state = State {
        pool: r2d2::Pool::new(manager)?,
        dir: Arc::new(TempDir::with_prefix("slack-")?),
    };
    println!(
        "Serving Slack messages from database \"{}\" on address {} using disk cache \"{}\"",
        db_path.display(),
        address,
        state.dir.path().to_str().unwrap_or("unrenderable path"),
    );
    Ok(actix_web::HttpServer::new(move || {
        actix_web::App::new()
            .app_data(actix_web::web::Data::new(state.clone()))
            .service(handle_index)
            .service(handle_channel)
            .default_service(actix_web::web::to(not_found))
    })
    .bind(address)?
    .run()
    .await?)
}
